// ---------------------------------------------------------------------------------------------------------------------
// Get PoGo portal data into RethinkDB
//
// @module DataConverter
// ---------------------------------------------------------------------------------------------------------------------

var s3 = require('s3');
var _ = require('lodash');
var r = require('rethinkdb');
var Promise = require('bluebird');

var client = s3.createClient({
    s3Options: {
        accessKeyId: 'AKIAJTFHGQQ6V3HF2HVQ',
        secretAccessKey: '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',
        region: 'ap-southeast-2'
    }
});

var requestParams = {
    localFile: 'pogo.json',
    s3Params: {
        Bucket: 'pogo-portals',
        Key: 'pogodata.txt'
    }
};

var downloader = client.downloadBuffer(requestParams.s3Params);

// var downloader = client.downloadFile(requestParams);

downloader.on('error', function(err)
{
    console.error("unable to download:", err.stack);
});

downloader.on('progress', function()
{
    console.log("progress", downloader.progressAmount, downloader.progressTotal);
});

downloader.on('end', function(buffer)
{
    var decoded = buffer.toString('utf8');

    console.log('Head:', decoded.slice(0, 100));

    var obj = JSON.parse(decoded);
    console.log("done downloading");

    formatData(obj)
        .then(function(finalArray)
        {
            updateRethink(finalArray);
        });
});

function formatData(data)
{
    return Promise.join(
        Promise.map(_.values(data.portals['id1461999480084'].pogo), function(gym)
        {
            var pointArray = _.map(gym.latlng.split(','), function(num)
            {
                return parseFloat(num);
            }).reverse();

            return {
                id: gym.guid,
                name: gym.label,
                point: r.point(pointArray[0], pointArray[1]),
                type: 'gym'
            };
        }),
        Promise.map(_.values(data.portals['id1461999480079'].pogo), function(pokestop)
        {
            var pointArray = _.map(pokestop.latlng.split(','), function(num)
            {
                return parseFloat(num);
            }).reverse();

            return {
                id: pokestop.guid,
                name: pokestop.label,
                point: r.point(pointArray[0], pointArray[1]),
                type: 'pokestop'
            };
        }),
        Promise.map(_.values(data.portals['id1462324832172'].pogo), function(none)
        {
            var pointArray = _.map(none.latlng.split(','), function(num)
            {
                return parseFloat(num);
            }).reverse();

            return {
                id: none.guid,
                name: none.label,
                point: r.point(pointArray[0], pointArray[1]),
                type: 'none'
            };
        }),
        function(gyms, pokestops, nones)
        {
            return gyms.concat(pokestops, nones);
        }
    );
} // end formatData

function updateRethink(data)
{
    data = _.filter(data, function(portal)
    {
        return !_.isUndefined(portal.name);
    });

    r.connect({ host: 'pinkie.skewedaspect.com', db: 'pgo' }, function(err, conn)
    {
        if(err)
        {
            throw err;
        }

        r.table('portal').insert(data, {
            conflict: function(id, oldDoc, newDoc)
            {
                if(oldDoc.type == 'unknown')
                {
                    return newDoc;
                }

                return oldDoc;
            }
        }).run(conn, function(err, res)
        {
            if(err)
            {
                throw err;
            }

            console.log('Rethink response:', res);
        });
    });

} // end updateRethink

// ---------------------------------------------------------------------------------------------------------------------

module.exports = {}; // end exports

// ---------------------------------------------------------------------------------------------------------------------